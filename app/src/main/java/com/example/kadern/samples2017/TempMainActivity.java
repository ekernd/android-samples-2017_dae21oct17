package com.example.kadern.samples2017;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class TempMainActivity extends AppCompatActivity {

    Button btnImgSpinner;
    Button btnIntentSender;
    Button btnUserDetails;
    Button btnAdapters;


    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int buttonResourceId = view.getId();
            switch(buttonResourceId){
                case R.id.btnImgSpinner:
                    startActivity(new Intent(TempMainActivity.this, ImageSpinnerActivity.class));
                    break;
                case R.id.btnIntentSender:
                    startActivity(new Intent(TempMainActivity.this, IntentSenderActivity.class));
                    break;
                case R.id.btnUserDetails:
                    startActivity(new Intent(TempMainActivity.this, UserDetailsActivity.class));
                    break;
                case R.id.btnAdapters:
                    startActivity(new Intent(TempMainActivity.this, Adapters.class));
                    break;
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_main);

        btnImgSpinner = (Button)findViewById(R.id.btnImgSpinner);
        btnImgSpinner.setOnClickListener(listener);

        btnIntentSender = (Button)findViewById(R.id.btnIntentSender);
        btnIntentSender.setOnClickListener(listener);

        btnUserDetails = (Button)findViewById(R.id.btnUserDetails);
        btnUserDetails.setOnClickListener(listener);

        btnAdapters = (Button)findViewById(R.id.btnAdapters);
        btnAdapters.setOnClickListener(listener);

    }


}
